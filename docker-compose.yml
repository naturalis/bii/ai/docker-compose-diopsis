version: "3.8"

services:
  traefik:
    image: registry.gitlab.com/naturalis/lib/docker/traefik:${TRAEFIK_VERSION:-latest}
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_traefik"
    restart: ${CONTAINER_RESTART:-unless-stopped}
    depends_on:
      - docker-socket-proxy
      - diopsis-imageserver
      - diopsis-statusserver
    networks:
      - docker-socket-proxy
      - webserver
    ports:
      - "${HOST_IP:-0.0.0.0}:80:80"
      - "${HOST_IP:-0.0.0.0}:443:443"
      - "127.0.0.1:${TRAEFIK_API_PORT:-8079}:8080"
    volumes:
      - "./letsencrypt:/letsencrypt"
      - "./traefik/traefik-dynamic-config.toml:/traefik-dynamic-config.toml"
    command:
      - --global.sendAnonymousUsage=false
      - --log.level=${TRAEFIK_LOG_LEVEL:-ERROR}
      - --api=${TRAEFIK_API_ENABLED:-false}
      - --api.insecure=true
      - --entrypoints.web.address=:80
      - --entrypoints.web.http.redirections.entrypoint.to=websecure
      - --entrypoints.web.http.redirections.entrypoint.scheme=https
      - --entrypoints.web.http.redirections.entrypoint.permanent=true
      - --entrypoints.websecure.address=:443
      - --certificatesresolvers.route53.acme.dnschallenge=true
      - --certificatesresolvers.route53.acme.dnschallenge.provider=route53
      - --certificatesresolvers.route53.acme.storage=/letsencrypt/route53.json
      - --providers.file.filename=/traefik-dynamic-config.toml
      - --providers.docker=true
      - --providers.docker.exposedbydefault=false
      - --providers.docker.endpoint=tcp://docker-socket-proxy:2375
      - --providers.docker.network=docker-socket-proxy
    env_file:
      - .env

  docker-socket-proxy:
    image: tecnativa/docker-socket-proxy:${DOCKERPROXY_VERSION:-latest}
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_docker-socket-proxy"
    restart: unless-stopped
    networks:
      - docker-socket-proxy
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro,delegated
    environment:
      CONTAINERS: 1

  diopsis-statusserver:
    image: $STATUSSERVER_CONTAINER
    restart: unless-stopped
    environment:
      - PYTHONPATH=${STATUSSERVER_PYTHONPATH}
      - STATIC_URL=${STATUSSERVER_STATIC_URL}
      - DEBUG=${STATUSSERVER_DEBUG}
      - PERSISTENCE_FOLDER_PATH=${STATUSSERVER_PERSISTENT_FOLDER_PATH}
    networks:
      - webserver
    labels:
      - traefik.enable=true
      - traefik.docker.network=${COMPOSE_PROJECT_NAME:-composeproject}_webserver
      - traefik.http.routers.statusserver.entrypoints=websecure
      - traefik.http.routers.statusserver.rule=Host(`${STATUSSERVER_FQDN:-replace.me.dryrun.link}`)  && PathPrefix(`${STATUSSERVER_PATH_PREFIX}`)
      - traefik.http.services.statusserver.loadbalancer.server.port=6002
      - traefik.http.routers.statusserver.tls.certresolver=route53
      - traefik.http.routers.statusserver.middlewares=statusserver-stripprefix@docker,hsts
      - traefik.http.middlewares.statusserver-stripprefix.stripprefix.prefixes=${STATUSSERVER_PATH_PREFIX}
      # hsts
      - traefik.http.middlewares.hsts.headers.stsSeconds=31536000
      - traefik.http.middlewares.hsts.headers.stsIncludeSubdomains=true
      - traefik.http.middlewares.hsts.headers.stsPreload=true
      - traefik.http.middlewares.hsts.headers.forceSTSHeader=true
    volumes:
      - "data:${STATUSSERVER_PERSISTENT_FOLDER_PATH}"


  diopsis-imageserver:
    image: $IMAGESERVER_CONTAINER
    restart: unless-stopped
    environment:
      - IMAGES_ROOT_FOLDER=${IMAGESERVER_IMAGES_ROOT_FOLDER}
      - PYTHONPATH=${IMAGESERVER_PYTHONPATH}
      - STATIC_URL=${IMAGESERVER_STATIC_URL}
      - DEBUG=${IMAGESERVER_DEBUG}
      - IMAGES_USERNAME=${IMAGESERVER_IMAGES_USERNAME}
      - IMAGES_PASSWORD=${IMAGESERVER_IMAGES_PASSWORD}
    networks:
      - webserver
    labels:
      - traefik.enable=true
      - traefik.docker.network=${COMPOSE_PROJECT_NAME:-composeproject}_webserver
      - traefik.http.routers.imageserver.entrypoints=websecure
      - traefik.http.routers.imageserver.rule=Host(`${IMAGESERVER_FQDN:-replace.me.dryrun.link}`)
      - traefik.http.services.imageserver.loadbalancer.server.port=6003
      - traefik.http.routers.imageserver.tls.certresolver=route53
      # hsts
      - traefik.http.routers.imageserver.middlewares=hsts
      - traefik.http.middlewares.hsts.headers.stsSeconds=31536000
      - traefik.http.middlewares.hsts.headers.stsIncludeSubdomains=true
      - traefik.http.middlewares.hsts.headers.stsPreload=true
      - traefik.http.middlewares.hsts.headers.forceSTSHeader=true
    volumes:
      - "data:${IMAGESERVER_IMAGES_ROOT_FOLDER}"

volumes:
  data:
    driver_opts:
      type: "nfs"
      o: "addr=${NFS_SERVER},nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport,${EFS_WRITABLE:-,ro}"
      device: ":/"

networks:
  docker-socket-proxy:
    internal: true
  webserver:
    internal: false
